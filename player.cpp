#include "player.h"
#include <stdlib.h>
#include <stdio.h>

/// Slorgers only plz
/// no non slorgers allowed

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;

    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
	 /*
	this->bord = (int *)malloc(8 * sizeof(int));
	for(int i = 0; i < 64; i++)
	{
		this->bord[i] = -1;
	}
	this->bord[27] = WHITE;
	this->bord[28] = BLACK;
	this->bord[35] = BLACK;
	this->bord[36] = WHITE;*/
	this->side = side;
	this->b = new Board();
	this->testingMinimax = 0;
}

/*
 * Destructor for the player.
 */
Player::~Player() {
	//free(this->bord);
	delete this->b;
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */

	 //so like we update the board like you know
	this->b->doMove(opponentsMove, (Side)((this->side + 1) % 2));
	//get the movez nd shit
	//Move *lemove = this->b->randMove(this->side);
	Move **validMovez = this->b->validMovez(this->side);
	long long nummoves = ((long long)validMovez[0]);
	if(nummoves == 0)
	{
		return NULL;
	}
	Move **moves = validMovez + 1;
	//now we will like do this shit
	int bestscore = -65;
	Move *bestmove = NULL;
	for(int i = 0; i < nummoves; i++)
	{
		int currentscore;
		if(this->testingMinimax)
		{
			currentscore = this->mmscore(this->b->copy(), moves[i], this->side, 3);
		}
		else
		{
			//use our heuristic thingy
			currentscore = this->heuristikskore(this->b->copy(), moves[i]);
		}
		if(currentscore > bestscore)
		{
			bestscore = currentscore;
			bestmove = moves[i];
		}
	}
	this->b->doMove(bestmove, this->side);
	free(validMovez);
	return bestmove;
}

int Player::heuristikskore(Board *b, Move *m)
{
	int prev = bordcount(b);
	b->doMove(m, this->side);
	int next = bordcount(b);
	delete b;
	int skore = next - prev;
	int xe = (m->x == 0 || m->x == 7);
	int ye = (m->y == 0 || m->y == 7);
	if(xe && ye)
	{
		return (skore > 0) ? skore * 3 : skore / 3;
	}
	int xne = (m->x == 1 || m->x == 6);
	int yne = (m->y == 1 || m->y == 6);
	if(xe || ye)
	{
		if(xne || yne)
		{
			return (skore > 0) ? skore / 3 : skore * 3;
		}
		return (skore > 0) ? skore * 2: skore / 2;
	}
	return skore;
}

int Player::bordcount(Board *b)
{
	return b->count(this->side) - b->count((Side)((this->side + 1) % 2));
}


int Player::mmscore(Board *b, Move *m, Side cm, int depth)
{
	if(depth == 0)
	{
		Side ts = this->side;
		Side os = (Side)((ts + 1) % 2);
		this->side = os;
		int therskore = heuristikskore(b, NULL);
		this->side = ts;
		int rskore = heuristikskore(b, NULL);
		return rskore - therskore;
		//return bordcount(b);
	}
	//do the move
	b->doMove(m, cm);
	Side cmn = (Side)((cm + 1) % 2);
	//get the valid moves
	Move **validMovez = b->validMovez(cmn);
	long long nummoves = ((long long)validMovez[0]);
	Move **moves = validMovez + 1;
	if(nummoves == 0)
	{
		return this->mmscore(b->copy(), NULL, cmn, depth - 1);
	}
	int bestscore = 65;
	for(int i = 0; i < nummoves; i++)
	{
		int currentscore = this->mmscore(b->copy(), moves[i], cmn, depth - 1);
		if(currentscore < bestscore)
		{
			bestscore = currentscore;
		}
	}
	delete b;
	free(validMovez);
	return bestscore;
}
