#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
using namespace std;

class Player {

public:
    Player(Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);
	int mmscore(Board *b, Move *m, Side cm, int depth);
	int bordcount(Board *b);
	int heuristikskore(Board *b, Move *m);
	//Move **getValidMovez();
	//int izValidMove(int move);
	//int slorg(int direction, int current, int frist);

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
	//int *bord;
	Side side;
	Board *b;
};

#endif
